import Vue from 'vue'
import Vuex from 'vuex'

// main.js

import VueSweetalert2 from 'vue-sweetalert2';

import 'sweetalert2/dist/sweetalert2.min.css';



Vue.use(Vuex)
Vue.use(VueSweetalert2);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
